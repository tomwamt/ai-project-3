from collections import Counter
from math import log
from random import shuffle

# ID3 Decision Tree node
class Node():
	def __init__(self, att, data):
		self.test = att
		self.children = {}
		self.data = data

# implementation of Decision-Tree-Learning found in Russel&Norvig p702
def create_tree(data, attrs, domains, parent=None):
	if len(data) == 0: # no more data
		return plurality(parent)
	elif all(data[i].dclass == data[i+1].dclass for i in range(len(data)-1)): # data is homogenous
		return data[0].dclass
	elif len(attrs) == 0: # no more attributes to split
		return plurality(data)
	else:
		att = max(attrs, key=lambda a: importance(a, data)) # argmax importance(a)
		tree = Node(att, data) # new tree node
		for value in domains[att]: # subdivide
			subdata = [ex for ex in data if ex.features[att] == value] # filter data on value
			subtree = create_tree(subdata, attrs - {att}, domains, data) # recursive call
			tree.children[value] = subtree # make subtree a child
		return tree

# counts the classes in the set, returns the highest counted class
def plurality(examples):
	c = Counter()
	for ex in examples:
		c[ex.dclass] += 1
	return max(c, key=lambda e: c[e])

# implementation of gain-ratio
def importance(att, data):
	iv = intrinsic_value(att, data)
	if iv == 0: # only 1 split in data
		return 0
	else:
		return gain(att, data)/iv

# implementation of entropy function
def entropy(data):
	counts = Counter() # count of classes
	total = len(data)

	for dp in data:
		counts[dp.dclass] += 1

	# implements P(c)log_2(P(c))
	def f(c):
		p = counts[c]/total
		return p*log(p, 2)

	# -sum over C: P(c)log_2(P(c))
	return -sum(f(c) for c in counts)

# implementation of gain function
def gain(att, data):
	total = len(data)
	result = entropy(data) # start with entropy of whole data set
	for value in set(dp.features[att] for dp in data): # for every possible value of att
		t = [dp for dp in data if dp.features[att] == value] # t = subset of data where att=value
		pt = len(t)/total # P(t)
		Ht = entropy(t) # entropy(t)
		result -= pt*Ht # subtract away P(t)H(t)
	return result

# implementation of IV function
def intrinsic_value(att, data):
	counts = Counter() # count of values for att
	total = len(data)

	for dp in data:
		counts[dp.features[att]] += 1

	# implements P(v)log_2(P(v))
	def f(v):
		p = counts[v]/total
		return p*log(p, 2)

	# -sum over v in Att: P(v)log_2(P(v))
	return -sum(f(v) for v in counts)

# wrapper for create_tree to create it then prune it
def create_pruned_tree(data, domains, pruneSize):
	# subdivide data into pruning set and test set (assume it is already shuffled)
	pruning = data[:pruneSize]
	training = data[pruneSize:]
	if __name__ == '__main__':
		print('Training on {} points.'.format(len(training)))
		print('Pruning validation with {} points.'.format(len(pruning)))

	attrs = set(range(len(data[0].features))) # determine the set of attributes
	tree = create_tree(training, attrs, domains)

	return prune(tree, pruning) # take the shears to the new tree

def prune(tree, test):
	# calculate accuracy of the tree as it is now
	base = prune_test(tree, test)
	# identify pruning possibilities
	# assume that we won't ever prune the root (that's just silly)
	pruneNodes = [] # set of (parent, branch) tuples: node at branch is what gets replaced
	find_nodes(tree, pruneNodes)

	if len(pruneNodes) > 0: # if there's anything to prune...
		# calculate accuracy of tree with branch removed
		def accf(parent, branch):
			old = parent.children[branch]
			parent.children[branch] = plurality(old.data)
			result = prune_test(tree, test)
			parent.children[branch] = old
			return result

		# pick the branch that maximizes accuracy when pruned
		parent, branch, acc = max(((parent, branch, accf(parent, branch)) for parent, branch in pruneNodes), key=lambda e: e[2])
		if acc > base: # if it's better than the original...
			# do the for real prune
			parent.children[branch] = plurality(parent.children[branch].data)
			# try to improve even further
			return prune(tree, test)

	return tree

# run the pruning set through the tree, record accuracy
def prune_test(tree, test):
	correct = 0
	total = len(test)
	for dp in test:
		try:
			dclass = id3_classify(tree, dp)
			if dp.dclass == dclass:
				correct += 1
		except KeyError: # tried to go down a branch that doesn't exist???
			pass
	return correct/total

# returns a list of (parent, edge) pairs to represent all inner nodes, except root
def find_nodes(tree, li):
	for child in tree.children:
		subtree = tree.children[child]
		if isinstance(subtree, Node):
			li.append((tree, child))
			find_nodes(subtree, li)

# navigates the tree to classify
def id3_classify(tree, query):
	current = tree
	while isinstance(current, Node): # while not at a class node
		current = current.children[query.features[current.test]]
	return current

def print_tree(tree, indent=4):
		print('test on attribute', tree.test)
		for branch in tree.children:
			print(' '*indent + '= {}: '.format(branch), end='')
			if isinstance(tree.children[branch], Node):
				print_tree(tree.children[branch], indent+4)
			else:
				print(tree.children[branch])

if __name__ == '__main__':
	import loader

	print('ID3 Decision Tree:')
	print()
	print('Loading House Votes data set...')
	data = loader.load_votes()
	shuffle(data)
	train = data[:-1]
	test = data[-1]
	
	tree = create_pruned_tree(train, [[-1, 0, 1]]*16, int(0.2*len(train)))
	print('Pruned Tree:')
	print_tree(tree)
	print()
	print('Query Point: {}'.format(test.features))
	dclass = id3_classify(tree, test)
	print('ID3 classification: {}'.format(dclass))
	print('True class: {}'.format(test.dclass))