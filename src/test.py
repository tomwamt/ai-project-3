import loader
import nb
import knn
import id3
import tan
from random import shuffle

# separates a list into groups based on a key function
def groupby(iterable, key=lambda x: x):
	groups = {}
	for item in iterable:
		k = key(item)
		if k in groups:
			groups[k].append(item)
		else:
			groups[k] = [item]
	return groups

# splits the data into two even, stratified sets
def split(data):
	shuffle(data)
	classes = groupby(data, key=lambda e: e.dclass)
	splits = ([], [])
	for cdata in classes.values():
		s = int(len(cdata)/2)
		splits[0].extend(cdata[:s])
		splits[1].extend(cdata[s:])

	shuffle(splits[0])
	shuffle(splits[1])
	
	return splits

# runs 5x2 cross validation on all 4 algorithms at once on a given data set
def test_data(data, name=''):
	# number correct for each
	nbc = 0
	knnc = 0
	id3c = 0
	tanc = 0
	# total query points
	total = 0
	for i in range(5):
		sp = split(data)
		# first cross
		total += len(sp[1])
		nbc += test_naive_bayes(*sp)
		knnc += test_knn(*sp, 5) # k = 5
		id3c += test_id3(*sp, 0.2) # pruning set is 20% of training set
		tanc += test_tan(*sp)
		# swap sets
		sp = (sp[1], sp[0])
		# second cross
		total += len(sp[1])
		nbc += test_naive_bayes(*sp)
		knnc += test_knn(*sp, 5)
		id3c += test_id3(*sp, 0.2)
		tanc += test_tan(*sp)
		#print('.', end='', flush=True)

	# print()
	# print('Naive Bayes: {:.2%}'.format(nbc/total))
	# print('k-NN: {:.2%}'.format(knnc/total))
	# print('ID3: {:.2%}'.format(id3c/total))
	# print('TAN: {:.2%}'.format(tanc/total))
	# print()
	print('{} & {:.2%} & {:.2%} & {:.2%} & {:.2%} \\\\'.format(name, knnc/total, nbc/total, tanc/total, id3c/total).replace('%', '\\%'))

def test_naive_bayes(training, test):
	numCorrect = 0
	
	priors = nb.ProbTables(training)
	for testDp in test:
		dclass = nb.naive_bayes_classify(priors, testDp)
		if dclass == testDp.dclass:
			numCorrect += 1
			
	return numCorrect

def test_knn(training, test, k):
	numCorrect = 0

	# normalize data
	normdata, means, stddevs = knn.normalize(training)
	for testDp in test:
		# normalize query
		normq = loader.DataPoint(tuple(0 if stddevs[i] == 0 else (testDp.features[i]-means[i])/stddevs[i] for i in range(len(testDp.features))))
		dclass = knn.knn_classify(normdata, normq, k)
		if dclass == testDp.dclass:
			numCorrect += 1

	return numCorrect

def test_id3(training, test, prunePerc):
	pruneSize = int(len(training) * prunePerc)

	n = len(training[0].features)
	# find out what the domains of each attribute are
	domains = [set(dp.features[a] for dp in training) | set(dp.features[a] for dp in test) for a in range(n)]
	tree = id3.create_pruned_tree(data, domains, pruneSize)

	numCorrect = 0
	for testDp in test:
		dclass = id3.id3_classify(tree, testDp)
		if dclass == testDp.dclass:
			numCorrect += 1

	return numCorrect

def test_tan(training, test):
	numCorrect = 0
	counts = tan.create_counts(training)
	tree = tan.create_tree(*counts)
	probs = tan.ProbTables(tree, *counts)
	for testDp in test:
		dclass = tan.tan_classify(tree, probs, testDp)
		if dclass == testDp.dclass:
			numCorrect += 1

	return numCorrect

def k_tune(datas, k):
	print('{}'.format(k), end='', flush=True)

	for data in datas:
		knnc = 0
		total = 0

		for i in range(5):
			sp = split(data)
			# first cross
			total += len(sp[1])
			knnc += test_knn(*sp, k) # k = 5
			# swap sets
			sp = (sp[1], sp[0])
			# second cross
			total += len(sp[1])
			knnc += test_knn(*sp, k)

		print(' & {:.2%}'.format(knnc/total).replace('%', r'\%'), end='', flush=True)
	print(r' \\')

# Load and test the data sets
if __name__ == '__main__':
	print(r'\begin{table}[ht]')
	print(r'\centering')
	print(r'\caption{Comparitive performance of the four algorithms.}')
	print(r'\label{perf}')
	print(r'\begin{tabular}{|c|r r r r|}')
	print(r'\hline')
	print(r'Data set & k-NN & Naive Bayes & TAN & ID3')
	print(r'\hline')

	#print('Cancer: ', end='', flush=True)
	data = loader.load_cancer([1]*9)
	test_data(data, 'Cancer')

	#print('Glass: ', end='', flush=True)
	data = loader.load_glass([x*1 for x in [0.003, 0.817, 1.442, 0.499, 0.775, 0.652, 1.423, 0.497, 0.097]])
	test_data(data, 'Glass')

	#print('Votes: ', end='', flush=True)
	data = loader.load_votes()
	test_data(data, 'Votes')

	#print('Iris: ', end='', flush=True)
	data = loader.load_iris([x*1 for x in [0.83, 0.43, 1.76, 0.76]]) # [1.66, 0.86, 3.52, 1.52]
	test_data(data, 'Iris')

	#print('Soybean: ', end='', flush=True)
	data = loader.load_soybean()
	test_data(data, 'Soybean')

	print(r'\hline')
	print(r'\end{tabular}')
	print(r'\end{table}')

	# data1 = loader.load_cancer([1]*9)
	# data2 = loader.load_glass([x*1 for x in [0.003, 0.817, 1.442, 0.499, 0.775, 0.652, 1.423, 0.497, 0.097]])
	# data3 = loader.load_votes()
	# data4 = loader.load_iris([x*1 for x in [0.83, 0.43, 1.76, 0.76]]) # [1.66, 0.86, 3.52, 1.52]
	# data5 = loader.load_soybean()
	# datas = [data1, data2, data3, data4, data5]

	# print(r'\begin{table}[ht]')
	# print(r'\centering')
	# print(r'\caption{k-NN accuracy on data sets with varying k-values.}')
	# print(r'\label{kTune}')
	# print(r'\begin{tabular}{r|c c c c c }')
	# print(r'k & Cancer & Glass & Votes & Iris & Soybean \\')
	# print(r'\hline')
	# for k in range(1,26,2):
	# 	k_tune(datas, k)
	# print(r'\end{tabular}')
	# print(r'\end{table}')