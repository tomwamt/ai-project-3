from collections import Counter
from itertools import product
from math import log

TINY = 1e-30

# create the various counts of the data that we need
def create_counts(data):
	classCounts = Counter() # C(c)
	featCounts = Counter() # C(x, c), C(x1, x2, c)
	
	n = len(data[0].features)
	attrDomains = [set() for i in range(n)] # will accumulate the domain for each attribute

	for dp in data:
		classCounts[dp.dclass] += 1 # count the class
		for i in range(n): # for each attribute
			attrDomains[i].add(dp.features[i])
			featCounts[(i, dp.features[i], dp.dclass)] += 1 # count that attribute i had value x with class c
		for i, j in product(range(n), range(n)): # for each attribute pair
			if i != j: # I said attribute pair, not the same one twice!
				featCounts[(i, dp.features[i], j, dp.features[j], dp.dclass)] += 1

	return len(data), n, classCounts, featCounts, attrDomains # return a whole bunch of useful stats
			
# creates the tan
def create_tree(lenData, n, classCounts, featCounts, attrDomains):
	# Conditional mutual information function
	def cmi(a1, a2): # class variable is implicit
		# P(x1,xj2,c)log_2(P(x1,x2|c)/(P(x1|c)P(x2|c)))
		def f(x1, x2, c):
			p1 = featCounts[(a1, x1, a2, x2, c)]/lenData # P(x1, x2, c)
			p2 = featCounts[(a1, x1, a2, x2, c)]/classCounts[c] # P(x1, x2 | c)
			p3 = featCounts[(a1, x1, c)]/classCounts[c] # P(x1 | c)
			p4 = featCounts[(a2, x2, c)]/classCounts[c] # P(x2 | c)
			if p2 == 0 or p3 == 0 or p4 == 0:
				return 0.0
			return p1*log(p2/(p3*p4), 2)
		return sum(f(x1, x2, c) for x1, x2, c in product(attrDomains[a1], attrDomains[a2], classCounts.keys()))

	# create half of weighted graph as adj. matrix
	graph = [[cmi(i, j) if i < j else -1 for i in range(n)] for j in range(n)]
	# graph is symmetric so copy one half to the other
	for i, j in product(range(n), range(n)):
		if i > j:
			graph[j][i] = graph[i][j]

	# Kruskal's Algorithm
	forest = [([i], []) for i in range(n)]
	while len(forest) > 1:
		# pick the maximum edge
		e = max(((i, j) for i in range(n) for j in range(n) if i < j), key=lambda e: graph[e[0]][e[1]])
		# find the trees that the edge connects
		for tree in forest:
			if e[0] in tree[0]:
				first = tree
			elif e[1] in tree[0]:
				second = tree
		# merge the trees
		first[0].extend(second[0])
		first[1].extend(second[1])
		first[1].append(e)
		# delete any other edges that connected the trees
		for i, j in product(range(n), range(n)):
			if i in first[0] and j in first[0] and i != j:
				graph[i][j] = -1
		# purge all trace that the second tree ever existed (Big Brother is watching you)
		forest.remove(second)

	tree = forest[0]
	# Orient edges
	tops = [0] # 0 will be root
	edges = set(tree[1]) # edges left to inspect
	dirEdges = [] # final result
	while len(tops) > 0:
		top = tops.pop() # kinda catchy
		adj = {e for e in edges if e[0] == top or e[1] == top} # find edges that connect to top
		for e in adj:
			edges.remove(e)
			if e[1] == top: # flip it if it needs to be flipped
				e = (e[1], e[0])
			dirEdges.append(e)
			tops.append(e[1])

	return dirEdges

def print_graph(graph):
	for row in graph:
		for val in row:
			print('{:.2f}  '.format(val), end='')
		print()

# structure to hold all useful probabilities
class ProbTables():
	def __init__(self, tree, lenData, n, classCounts, featCounts, attrDomains):
		self.pClass = {} # P(c)
		self.pRoot = {} # P(x_root | c)
		self.pFeat = {} # P(x1 | x2, c)

		for c in classCounts:
			self.pClass[c] = classCounts[c]/lenData # calculate P(c)
			for v in attrDomains[0]:
				self.pRoot[(v, c)] = featCounts[(0, v, c)]/classCounts[c] # calculate P(x_root | c)
			for t, h in tree: # for every (tail, head) in tree
				for hv, tv in product(attrDomains[h], attrDomains[t]): 
					if featCounts[(t, tv, c)] != 0: # avoid a div by 0
						self.pFeat[(h, hv, t, tv, c)] = featCounts[(h, hv, t, tv, c)]/featCounts[(t, tv, c)] # calculate P(x_head | x_tail, c)

def tan_classify(tree, probs, query):
	cprobs = {} # P(c | query)
	for c in probs.pClass:
		prob = probs.pClass[c] # Start with P(c)
		try:
			prob *= probs.pRoot[(query.features[0], c)] # multiply by P(x_root | c)
		except KeyError: # P(x_root | c) ~= 0
			prob *= TINY
		for t, h in tree:
			try:
				prob *= probs.pFeat[(h, query.features[h], t, query.features[t], c)] # multiply by P(x_head | x_tail, c)
			except KeyError: # P(x_head | x_tail, c) ~= 0
				prob *= TINY
		cprobs[c] = prob
		if __name__ == '__main__': print('\tP(class = {} | query) = {:.5%}'.format(c, prob))

	return max(cprobs, key=lambda e: cprobs[e]) # pick the max of the probs

if __name__ == '__main__':
	import loader
	from random import shuffle

	print('Tree Augmented Naive Bayes')
	print()
	bins = [x*1 for x in [0.83, 0.43, 1.76, 0.76]]
	print('Loading Iris data set... (Bin sizes: {})'.format(bins))
	data = loader.load_iris(bins)
	shuffle(data)
	train = data[:-1]
	test = data[-1]

	print('Training on {} points.'.format(len(train)))
	counts = create_counts(train)
	tree = create_tree(*counts)
	print('Tree edges (0 is root):')
	print(tree)
	print()
	probs = ProbTables(tree, *counts)
	print('Class prior probabilities: P(c)')
	for c in probs.pClass:
		print('\tP(class = {}) = {:.2%}'.format(c, probs.pClass[c]))
	print('Root feature probabilties given a class: P(x|c)')
	for key in probs.pRoot.keys():
		print('\tP(feature[0] = {} | class = {}) = {:.2%}'.format(*key, probs.pRoot[key]))
	print('Feature probabilities given a parent and a class: P(x1|x2,c)')
	for key in probs.pFeat.keys():
		print('\tP(feature[{}] = {} | feature[{}] = {}, class = {}) = {:.2%}'.format(*key, probs.pFeat[key]))
	print()

	print('Query Point: {}'.format(test.features))
	print('Probabilities of class given query (unnormalized):')
	dclass = tan_classify(tree, probs, test)
	print('TAN classification: {}'.format(dclass))
	print('True class: {}'.format(test.dclass))