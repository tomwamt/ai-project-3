from collections import Counter

TINY = 1e-30

# structure to hold calculated probabilities
class ProbTables():
	def __init__(self, data):
		self.pClass = {} # P(c) -- probability of a class
		self.pFeat = {} # P(f|c) -- probability of a feature given a class

		totalData = len(data)
		classCounts = Counter()
		featCounts = Counter()

		# count up all the data
		for dp in data:
			classCounts[dp.dclass] += 1 # C(c)
			for i in range(len(dp.features)):
				featCounts[(i, dp.features[i], dp.dclass)] += 1
			
		# Calculate P(c)	
		for c in classCounts:
			self.pClass[c] = classCounts[c]/totalData
			
		# Calculate P(x | c)
		for key in featCounts:
			dclass = key[2]
			self.pFeat[key] = featCounts[key]/classCounts[dclass]

def naive_bayes_classify(probs, query):
	cprobs = {} # Hold P(c | query) for each class c
	for c in probs.pClass:
		prod = probs.pClass[c] # start with P(c)
		for fi in range(len(query.features)):
			try:
				prod *= probs.pFeat[(fi, query.features[fi], c)] # multiply by P(xi | c)
			except KeyError: # P(xi | c) ~= 0
				prod *= TINY
		cprobs[c] = prod
		if __name__ == '__main__': print('\tP(class = {} | query) = {:.4%}'.format(c, prod))

	return max(cprobs, key=lambda e: cprobs[e]) # take the maximum of the probabilities

if __name__ == '__main__':
	import loader
	from random import shuffle

	print('Naive Bayes')
	print()

	bins = [x*1 for x in [0.83, 0.43, 1.76, 0.76]]
	print('Loading Iris data set... (Bin sizes: {})'.format(bins))
	data = loader.load_iris(bins)
	shuffle(data)
	train = data[:-1]
	test = data[-1]

	print('Training on: {} points.'.format(len(train)))
	probs = ProbTables(train)
	print('Class prior probabilities: P(c)')
	for c in probs.pClass:
		print('\tP(class = {}) = {:.2%}'.format(c, probs.pClass[c]))
	print('Feature probabilties given a class: P(x|c)')
	for key in probs.pFeat.keys():
		print('\tP(feature[{}] = {} | class = {}) = {:.2%}'.format(*key, probs.pFeat[key]))
	print()

	print('Query Point: {}'.format(test.features))
	print('Probabilities of class given query (unnormalized):')
	dclass = naive_bayes_classify(probs, test)
	print('Naive Bayes classification: {}'.format(dclass))
	print('True class: {}'.format(test.dclass))