from collections import Counter
from math import sqrt
from loader import DataPoint

# Minkowski distance metric (squared), p=2 (Geometric distance)
def d2(p1, p2):
	return sum(a**2 for a in (p1.features[i]-p2.features[i] for i in range(len(p1.features))))

# Normalize all data points so all attributes have mean=0 and stddev=1
def normalize(data):
	n = len(data[0].features) # number of attributes in data
	# calculate means
	mean = [sum(e.features[i] for e in data)/len(data) for i in range(n)]
	# calculate standard deviations
	stddev = [sqrt(sum((e.features[i] - mean[i])**2 for e in data)/len(data)) for i in range(n)]
	# new value for feature = (value-mean)/stddev
	newdata = [DataPoint(tuple(0 if stddev[i] == 0 else (dp.features[i]-mean[i])/stddev[i] for i in range(n)), dp.dclass) for dp in data]
	return newdata, mean, stddev

# find the k nearest neighbors to query point
# if data is normalized, query should be normalized with same means, stddevs
def NN(k, data, query):
	best = [] # keep track of the k best so far, sorted closest first
	for dp in data:
		dist = d2(dp, query)
		# loop through best, see if new dist is better than any
		i = 0
		for i, (_, d) in enumerate(best):
			if dist < d:
				break
		else: # loop didn't break
			i += 1
		best.insert(i, (dp, dist)) # insert it
		if len(best) > k: # if our best list is too long, get rid of the farthest
			best.pop()
	return [b[0] for b in best] # return the points (we don't care about the distances now)

# given a data set and a query, find the nearest neighbors and take the pluralty class of them
def knn_classify(data, query, k):
	# find k nearest neighbors
	neighbors = NN(k, data, query)
	# count up neighbors
	votes = Counter()
	for n in neighbors:
		votes[n.dclass] += 1
	return max(votes, key=lambda e: votes[e]) # pick the max count

# sample output
if __name__ == '__main__':
	import loader
	from random import shuffle

	print('k-Nearest Neighbor:')
	print()
	data = loader.load_cancer([1]*9)
	shuffle(data)
	ndata, _, _ = normalize(data)
	train = ndata[:-1]
	print('Training set: {} points.'.format(len(train)))
	test = ndata[-1]
	print('Query Point: ({})'.format(', '.join('{: .2f}'.format(f) for f in test.features)))
	neighbors = NN(5, train, test)
	print('5 nearest neighbors:')
	for n in neighbors:
		print('\t({}) \tclass: {} \td^2 = {:.2f}'.format(', '.join('{: .2f}'.format(f) for f in n.features), n.dclass, d2(test, n)))
	dclass = knn_classify(train, test, 5)
	print('k-NN classification: {}'.format(dclass))
	print('True class: {}'.format(test.dclass))