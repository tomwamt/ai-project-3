from random import randint

# simple structure to hold data features and class
class DataPoint():
	def __init__(self, features=(), dclass=None):
		self.features = tuple(features)
		self.dclass = dclass

# loads and discretizes the breast cancer data set
# technically the data values are already discrete, but they can be grouped
# into more general groups this way
def load_cancer(binSizes):
	f = open('../data/breast-cancer-wisconsin.data.txt')
	lines = f.readlines()
	f.close()

	data = []
	for line in lines:
		attrs = line.strip().split(',')
		#  1. Sample code number            id number
		#  2. Clump Thickness               1 - 10
		#  3. Uniformity of Cell Size       1 - 10
		#  4. Uniformity of Cell Shape      1 - 10
		#  5. Marginal Adhesion             1 - 10
		#  6. Single Epithelial Cell Size   1 - 10
		#  7. Bare Nuclei                   1 - 10
		#  8. Bland Chromatin               1 - 10
		#  9. Normal Nucleoli               1 - 10
		# 10. Mitoses                       1 - 10
		# 11. Class:                        (2 for benign, 4 for malignant)
		dp = DataPoint()
		feat = []
		for att, binSize in zip(attrs[1:10], binSizes):
			if att == '?': # handle missing attributes
				v = randint(1,10) # give it a uniformly random value
			else:
				v = int(att)
			feat.append(int(v//binSize)) # integer divide to fit into bins
		dp.features = feat
		dp.dclass = int(attrs[10])
		data.append(dp)

	return data

# loads and discretizes the glass data set
def load_glass(binSizes):
	f = open('../data/glass.data.txt')
	lines = f.readlines()
	f.close()

	data = []
	for line in lines:
		attrs = line.strip().split(',')
	  #  1. Id number: 1 to 214
	  #  2. RI: refractive index
	  #  3. Na: Sodium (unit measurement: weight percent in corresponding oxide, as 
	  #                 are attributes 4-10)
	  #  4. Mg: Magnesium
	  #  5. Al: Aluminum
	  #  6. Si: Silicon
	  #  7. K: Potassium
	  #  8. Ca: Calcium
	  #  9. Ba: Barium
	  # 10. Fe: Iron
	  # 11. Type of glass: (class attribute)
	  #     -- 1 building_windows_float_processed
	  #     -- 2 building_windows_non_float_processed
	  #     -- 3 vehicle_windows_float_processed
	  #     -- 4 vehicle_windows_non_float_processed (none in this database)
	  #     -- 5 containers
	  #     -- 6 tableware
	  #     -- 7 headlamps
		dp = DataPoint()
		feat = []
		for att, binSize in zip(attrs[1:10], binSizes):
			v = float(att)
			feat.append(int(v//binSize)) # integer divide to fit into bins
		dp.features = feat
		dp.dclass = int(attrs[10])
		data.append(dp)

	return data

# loads the vote data set (no discretization)
def load_votes():
	f = open('../data/house-votes-84.data.txt')
	lines = f.readlines()
	f.close()

	data = []
	for line in lines:
		attrs = line.split(',')
	  #  1. Class Name: 2 (democrat, republican)
	  #  2. handicapped-infants: 2 (y,n)
	  #  3. water-project-cost-sharing: 2 (y,n)
	  #  4. adoption-of-the-budget-resolution: 2 (y,n)
	  #  5. physician-fee-freeze: 2 (y,n)
	  #  6. el-salvador-aid: 2 (y,n)
	  #  7. religious-groups-in-schools: 2 (y,n)
	  #  8. anti-satellite-test-ban: 2 (y,n)
	  #  9. aid-to-nicaraguan-contras: 2 (y,n)
	  # 10. mx-missile: 2 (y,n)
	  # 11. immigration: 2 (y,n)
	  # 12. synfuels-corporation-cutback: 2 (y,n)
	  # 13. education-spending: 2 (y,n)
	  # 14. superfund-right-to-sue: 2 (y,n)
	  # 15. crime: 2 (y,n)
	  # 16. duty-free-exports: 2 (y,n)
	  # 17. export-administration-act-south-africa: 2 (y,n)
		
		feat = []
		for att in attrs[1:]:
			att = att.strip()
			if att == 'y':
				feat.append(1)
			elif att == 'n':
				feat.append(-1)
			else: # abstained votes: counted as a third possible value
				feat.append(0)
		dclass = attrs[0]
		dp = DataPoint(feat, dclass)
		data.append(dp)

	return data

# loads and discretizes the iris data set
def load_iris(binSizes):
	f = open('../data/iris.data.txt')
	lines = f.readlines()
	f.close()

	data = []
	for line in lines:
		if line.strip() == '':
			continue
		attrs = line.strip().split(',')
	   # 1. sepal length in cm
	   # 2. sepal width in cm
	   # 3. petal length in cm
	   # 4. petal width in cm
	   # 5. class: 
	   #    -- Iris Setosa
	   #    -- Iris Versicolour
	   #    -- Iris Virginica
		
		feat = []
		for att, binSize in zip(attrs[:4], binSizes):
			v = float(att)
			feat.append(int(v//binSize)) # integer divide to fit into bins
		dclass = attrs[4]
		dp = DataPoint(feat, dclass)
		data.append(dp)

	return data

# loads the soybean data set (no discretization)
def load_soybean():
	f = open('../data/soybean-small.data.txt')
	lines = f.readlines()
	f.close()

	data = []
	for line in lines:
		if line.strip() == '':
			continue
		attrs = line.split(',')
		# 35 attributes... unnamed
		feat = []
		for att in attrs[:-1]:
			v = int(att)
			feat.append(v)
		dclass = attrs[-1]
		dp = DataPoint(feat, dclass)
		data.append(dp)

	return data