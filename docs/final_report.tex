\documentclass{article}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{subcaption}
\usepackage{graphicx}
\graphicspath{{img/}}
\usepackage{algorithm}
\usepackage{algpseudocode}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
\usepackage[utf8]{inputenc}

\title{Experiments in Machine Learning \\ \large Final Report}
\author{Group 20:\\Jason Sanders, Travis Stone, Tom Warner}
\date{\today}

\begin{document}
\maketitle

\abstract{The results of the Machine Learning assignment showed that some algorithms clearly performed better than others. Iterative Dichotimizer 3 performed the best on all five data sets. However,  Tree-Augmented Naive Bayes was by far the worst overall performer; it never scored higher than third place, often classifying at a far worse accuracy than the others.  Nearest Neighbor and Naive Bayes had comparable performances, trailing just behind ID3.}
\section{Introduction}
\paragraph{}
For this project, we were tasked with designing and implementing four machine learning algorithms to perform supervised learning on five different data sets. The four algorithms that we tested were k-Nearest Neighbors, Naive Bayes, Tree-Augmented Naive Bayes, and Iterative Dichotimizer 3. Our goal in this paper is to analyze the four algorithms' performances on the five data sets and determine which algorithms perform strongest on which data sets.

\section{Hypothesis}
\paragraph{}
Going into this assignment, we believed that our results would follow the ``No Free Lunch'' Theorem, and each of the four algorithms would be useful on different data sets, but no algorithm will consistently perform better than the rest.

\paragraph{}
For the breast cancer data set and the glass data set, we believed that k-Nearest Neighbors would be the most effective at classifying the data. Based on some preliminary examination of the data, we believed that the breast cancer data set would have been tightly clustered into two very distinct groups. Similarly, the glass data set seemed to be grouped by the chemical makeup of the glass, which corresponds to the glass’s refractive index and its uses. We believed k-Nearest Neighbor will perform well with these clusters. 

\paragraph{}
For the Iris data set, we believed that the ID3 algorithm will be the best at accurately classifying the data. In the data set, Irises are divided up into three categories with a relatively small number of attributes describing them. We thought each of these variables would prove to be important to one another, and sometimes their effects on the result may intermingle. Based on this, we predicted that ID3’s ability to track the most important attributes in varying subsets of the data will produce the most accurate results.

\paragraph{}
For the soybean data set, we believed that the Tree-Augmented Naive Bayes algorithm would be the best at classifying the data. The soybean data had a large number of attributes, and some attributes likely depended on others. We predicted that TAN's tree model of dependent attributes should have proven helpful in classifying this data set.

\paragraph{}
In contrast, we believed that Naive Bayes will be the most effective at classifying the congress voting data set. The data set is neatly organized into whether a congressman is a republican or democrat followed by their record on 16 major votes. We thought Naive Bayes' probabilistic classifying would be useful, since a congressman's voting record was at least somewhat indicative of their party affiliation and allowed for the creation of a probability density distribution.

\section{Algorithms}
\subsection{k-Nearest Neighbors}
\paragraph{}
k-Nearest Neighbors (kNN) is a simple algorithm that uses some distance function to select the k closest data points from the training set to a queried data point and uses the plurality class of those k points to classify the query. kNN is a lazy classifier, building no model other than a data structure to hold and search for the training data.

\paragraph{}
kNN has the advantage of being simple and effective when the data naturally clusters and query points fall within these well-defined clusters. This makes it easy to decide a strong majority from neighboring points. However, kNN may fall short when data is not clustered. As a lazy method, it may be slow to run, especially with large data sets. A smart choice in data structure may partially mitigate that slowness.

\subsection{Naive Bayes}
\paragraph{}
The Naive Bayes algorithm uses the probabilities of data points having attributes of certain values to classify queries. Specifically, it first calculates several prior probabilities of classes having certain attributes based on the training data fed into it. When given a query data point, the algorithm uses these prior probabilities along with Bayes' Rule to calculate, for all classes, the conditional probability of the class given the evidence of the query data. The class with the highest probability is selected as the class label for the query point.

\paragraph{}
Naive Bayes makes the assumption that all attributes in the data set are conditionally independent given the class. In data sets where this assumption holds, Naive Bayes should perform well. However, this is a somewhat naive assumption to make (thus the name), as many data sets may include some sort of interdependence between the data attributes.

\subsection{Tree Augmented Naive Bayes}
\paragraph{}
A modification of Naive Bayes, Tree Augmented Naive Bayes (TAN) introduces a tree structure to represent and consider dependency relations between the data attributes in a data set. This tree structure is created from a maximal spanning tree of a Bayesian Network weighted by calculations of conditional mutual information between attributes. Once this tree is constructed, TAN proceeds to calculate the conditional probabilities of all classes given the evidence of a query point similar to Naive Bayes, but also factors in the additional dependency information.

\paragraph{}
Given that TAN is a more advanced version of Naive Bayes, it is expected that its performance should be at least as good as Naive Bayes. Additionally, because the assumption of independent attributes has been lifted, TAN should perform well on data sets that contain interdependent data points. However, because a tree structure is enforced, TAN may create edges where no dependency exists, or skip edges that strongly dependent.

\subsection{Iterative Dichotimizer 3}
\paragraph{}
Iterative Dichotimizer 3 (ID3) is a decision tree-based learning method. It is an eager method that first computes a model (in this case, a decision tree) and then uses that model to quickly classify query points. The tree is composed of nodes representing tests on a single attribute at a time, with edges representing partitions of the data set along that attribute. The tree is constructed in such a way that in each subtree, a test of the most important attribute -- as determined by a gain ratio -- is selected for the root node. The leaf nodes of the tree are class labels. When given a query data point, ID3 simply traverses the tree based on the attributes in the query point until it arrives at a leaf node, assigning the query that class label.

\paragraph{}
Because ID3 selects the most important attribute test first for a given subset of the data, it should be good at generalizing. Additionally, smart pruning techniques should improve the accuracy even further. However, ID3 may struggle when there is no clear ``most important'' attribute in the data, i.e. all of the attributes are equally important in determining the class of a point.

\section{Design Decisions}
\subsection{Missing Attributes}
\paragraph{}
For handling missing values in the breast cancer data set, we decided to use data imputation. The attributes that are missing values are measured from 1-10, so we will substitute a uniformly chosen random value between 1 and 10 for those missing values. Because missing data in the congress vote data set is not truly missing values, we will handle the unknown values in the data by treating them as a third possible data value (in addition to yes and no). We decided to do this because a gap in the data set indicates that the congressman either abstained or missed the vote, and imputing values for the gaps could cause issues with the algorithms.

\subsection{Discretization}
\paragraph{}
We decided to use simple fixed-distance binning to discretize the continuous data in the Glass and Iris data sets. This technique involved simply dividing the real-valued data by the given bin size for that attribute, then using the floor operation to obtain an integer value. This integer is then inserted into the data point in place of the real-valued attribute.

\paragraph{}
The discretization process happened while the data was was loaded from the data files, and the discretized data was passed in to all four algorithms.

\subsection{k-Nearest Neighbors}
\paragraph{}
For the k-Nearest Neighbor algorithm, we decided to not assign weights to data points relative to the query location. Each of the k neighbors contributed equally to the classification regardless of distance to the query point or other factors. This was for the sake of simplicity and efficiency.

\paragraph{}
We used a simple sequential search to search for the nearest neighbors. The simplicity of this strategy compared to a k-d tree search made it the more appealing choice. While this method is relatively inefficient, the size of the data sets were not so large that the algorithms runtime was severely affected. Given the scope of the assignment, we 

\paragraph{}
The distance function used our implementation of k-Nearest Neighbor was the Minkowski distance metric with $p = 2$, i.e.  Euclidean distance.

\paragraph{}
To avoid the problem of differing scales of the various attributes of a data set, we decided to normalize the data before finding neighbors. The attribute values of each of the data points were shifted and scaled so each attribute had a mean of zero and a standard deviation of one.

\subsection{Iterative Dichotimizer 3}
\paragraph{}
We will be implementing a reduced-error pruning technique to ensure that our decision trees are generalized and do not over-fit the data. This technique involves generating the tree to its fullest extent, then iteratively removing and generalizing subtrees where doing so reduces error when tested on a pruning set partition of the full data set.

\section{Experimental Method}
\paragraph{}
To evaluate the performance of the algorithms, we will be recording the results while using 5x2 cross validation. At the start of execution, we will split each of the data sets into two randomly selected, stratified partitions. The first set will be our training set - the data we use to establish the models in our algorithms. When that is established, we will run each algorithm on the second set, dubbed the test set, and accuracy will be measured. Then the training and test sets will be swapped, and the algorithms run again. This process will be repeated 5 times in total. The overall accuracy measure will be averaged over all 5 folds.

\paragraph{}
We measured accuracy by recording the number of test data points an algorithm correctly classified and dividing by the total size of the test set in order to determine the percentage of correct classifications.

\paragraph{}
Additionally, we ran the k-NN algorithm on the 5 data sets multiple time with varying $k$ values in order to determine how changing the $k$ value affected the accuracy of the algorithm.

\section{Results}

\begin{table}[ht]
\centering
\caption{Comparative performance of the four algorithms.}
\label{perf}
\begin{tabular}{c|r r r r}

Data set & k-NN & Naive Bayes & TAN & ID3 \\
\hline
Cancer & 96.37\% & 96.74\% & 73.19\% & 99.03\% \\
Glass & 58.97\% & 59.91\% & 50.19\% & 73.27\% \\
Votes & 93.15\% & 90.62\% & 90.44\% & 98.48\% \\
Iris & 93.33\% & 92.67\% & 84.40\% & 97.33\% \\
Soybean & 99.15\% & 100.00\% & 24.26\% & 100.00\% \\
\hline
\end{tabular}
\end{table}

\begin{table}[ht]
\centering
\caption{k-NN accuracy on data sets with varying k-values.}
\label{kTune}
\begin{tabular}{r|c c c c c}

k & Cancer & Glass & Votes & Iris & Soybean \\
\hline
1 & 94.76\% & 58.79\% & 92.28\% & 90.80\% & 100.00\% \\
3 & 96.05\% & 59.44\% & 93.61\% & 93.20\% & 99.57\% \\
5 & 96.22\% & 60.65\% & 92.92\% & 93.47\% & 100.00\% \\
7 & 96.68\% & 58.04\% & 92.83\% & 93.33\% & 97.02\% \\
9 & 96.74\% & 59.35\% & 92.83\% & 91.87\% & 90.21\% \\
11 & 96.65\% & 59.35\% & 91.86\% & 92.27\% & 60.85\% \\
13 & 96.51\% & 59.07\% & 92.00\% & 90.80\% & 41.70\% \\
15 & 96.42\% & 55.14\% & 91.86\% & 90.27\% & 36.17\% \\
17 & 96.45\% & 56.64\% & 91.36\% & 92.13\% & 36.17\% \\
19 & 95.99\% & 54.49\% & 91.31\% & 90.13\% & 36.17\% \\
21 & 96.22\% & 55.79\% & 91.03\% & 89.07\% & 36.17\% \\
23 & 96.17\% & 54.58\% & 90.85\% & 87.20\% & 36.17\% \\
25 & 96.02\% & 53.64\% & 90.62\% & 86.13\% & 36.17\% \\
\hline
\end{tabular}
\end{table}

\section{Performance Discussion}
\paragraph{}
To begin, our group's initial hypotheses were almost entirely incorrect. We made the assumption that each data set would respond better to a different method of machine learning based on the ``No Free Lunch'' concept. However, we saw terrible performances from Tree-Augmented Naive Bayes and fantastic ones from Iterative Dichotimizer 3 across the board. Nearest Neighbor and Naive Bayes performed somewhere around the middle.
\paragraph{}
The first thing that needs to be addressed is the comparably pitiful performance of TAN in comparison to the other three algorithms. We attribute this to TAN forming links between attributes where those connections may be weak or not there at all. This error could be prevented by hand-crafting a more sophisticated Bayesian Network for the data set, but the result would no longer be TAN.
\paragraph{}
In particular TAN did especially poorly on the Soybean data set. This is likely due to the small size of the data set compared to the large number of attributes in each data point; this lead to a large number of zero-weight edges in the Bayesian Network computed by conditional mutual information. The tree structure enforced by TAN created links along these edges that did not correspond to any real dependence.
\paragraph{}
Our group's best performer was Iterative Dichotomizer 3, which was unexpected; our group had assumed each method would perform better on some sets than the rest of the algorithms. We think this happened because ID3 is the algorithm that makes the fewest assumptions about the data. While Nearest Neighbor assumes that nearby nodes match the class of the queried node and Naive Bayes assumes conditional independence of attributes given classes, ID3 simply looks at the data makes decisions based off of it.
\paragraph{}
When considering of k-Nearest Neighbor, it is interesting to note the dropoff in performance as the number of neighbors selected increased. In a way, this was to be expected. The more data points that we take into account for every query, larger subsets of the training data are considered, and the less likely Nearest Neighbor is to decide which class the query point belongs to. In practice, this dropoff could be largely mitigated by weighting to each point's vote depending on the distance from the query point.
\paragraph{}
The performance of Naive Bayes is somewhere in between ID3 and Nearest Neighbor. It's accuracy is often comparable to k-Nearest Neighbor, but its performance isn't quite as impressive as ID3's. Its probability distributions leave it vulnerable to making more incorrect assumptions than ID3, but the curves are far from inaccurate until we get to the Glass data set.
\paragraph{}
The glass data set was a problem for each of the algorithms. The only method of classification that scored above a 60 percent average for accuracy was ID3, which came in at 73.27 percent. This could be the result of particularly noise-saturated data, or it could have something to do with the set having very low amounts of conditional dependence.
\section{References}
[1] S.  Russell, P.  Norvig and E.  Davis, \textit{Artificial Intelligence}. Upper Saddle River, NJ: Prentice Hall, 2010.\\[2pt]
[2] N. Friedman, D. Geiger, and M. Goldszmidt, "Bayesian Network Classifiers," \textit{Machine Learning}, no. 29, pp. 131–163, 1997.\\[2pt]
[3]"UCI Machine Learning Repository", 2016. [Online]. Available: http://archive.ics.uci.edu/ml/.
\end{document}
