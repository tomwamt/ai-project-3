\documentclass{article}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{subcaption}
\usepackage{graphicx}
\graphicspath{{img/}}
\usepackage{algorithm}
\usepackage{algpseudocode}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
\usepackage[utf8]{inputenc}

\title{Design Document \\ \large Project \#3}
\author{Group 20:\\Jason Sanders, Travis Stone, Tom Warner}
\date{\today}

\begin{document}
\maketitle

\section{Problem Statement}
\paragraph{}
For this project, we have been tasked with designing and implementing four machine learning algorithms to perform supervised learning on five different data sets. The four algorithms that we will be testing are k-Nearest Neighbors, Naive-Bayes, Tree-Augmented Naive Bayes, and Iterative Dichotimizer 3. Our goal is to analyze the four algorithms' performance on the five data sets and determine which algorithms perform strongest on which data sets. 

\subsection{Hypothesis}
\paragraph{}
We believe that our results will follow the ``No Free Lunch'' Theorem, and each of the four algorithms we be useful on different data sets, but no algorithm will consistently perform better than the rest.

\paragraph{}
For the breast cancer data set and the glass data set, we believe that k-Nearest Neighbors will be the most effective at classifying the data. Based on some preliminary examination of the data, we believe that the breast cancer data set will be tightly clustered into two very distinct groups. Similarly, the glass data set seems to be grouped by the chemical makeup of the glass, which corresponds to the glass’s refractive index and its uses. We believe k-Nearest Neighbor will perform well with these clusters. 

\paragraph{}
For the Iris data set, we believe that the ID3 algorithm will be the best at accurately classifying the data. In the data set, Irises are divided up into three categories with a relatively small number of attributes describing them. Each of these variables will prove to be important to one another, and sometimes their effects on the result may intermingle. Based on this, we predict that ID3’s ability to track the most important attributes in varying subsets of the data will produce the most accurate results.

\paragraph{}
For the soybean data set, we believe that the Tree-Augmented Naive Bayes algorithm will be the best at classifying the data. The soybean data has a large amount of attributes, and some attributes will likely depend on other attributes. TAN's tree model of dependent attributes should prove helpful in classifying this data set.

\paragraph{}
In contrast, we believe that Naive Bayes will be the most effective at classifying the congress voting data set. The data set is neatly organized into whether a congressman is a republican or democrat followed by their record on 16 major votes. Naive Bayes' probabilistic classifying will be useful, since a congressman's voting record will be at least somewhat indicative of their party affiliation and allow for the creation of a probability density distribution.

\section{Algorithms}
\subsection{k-Nearest Neighbors}
\paragraph{}
k-Nearest Neighbors (kNN) is a simple algorithm that uses some distance function to select the k closest data points from the training set to a queried data point and uses the plurality class of those k points to classify the query. kNN is a lazy classifier, building no model other than a data structure to hold and search for the training data.

\paragraph{}
kNN has the advantage of being simple and effective when the data naturally clusters and query points fall within these well-defined clusters. This makes it easy to decide a strong majority from neighboring points. However, kNN may fall short when data is not clustered. As a lazy method, it may be slow to run, especially with large data sets. A smart choice in data structure may partially mitigate that slowness.

\subsection{Naive Bayes}
\paragraph{}
Naive Bayes algorithm uses the probabilities of data points having certain values for its attributes to classify queries. Specifically, it first calculates several prior probabilities of classes having certain attributes based on the training data. When given a query data point, the algorithm uses these prior probabilities with Bayes' Rule to calculate, for all classes, the conditional probability that the query point falls within the class. The class with the highest probability is selected as the class label for the query point.

\paragraph{}
Naive Bayes makes the assumption that all attributes in the data set are independent. In data sets where this assumption holds, Naive Bayes should perform well. However, this is a somewhat naive assumption to make (thus the name), as most data sets will include some sort of interdependence between the data attributes.

\subsection{Tree Augmented Naive Bayes}
\paragraph{}
A modification of Naive Bayes, Tree Augmented Naive Bayes (TAN) introduces a tree structure to represent and consider dependency relations between the data attributes in a data set. This tree structure is created from a maximal spanning tree of a Bayesian Network weighted by calculations of conditional mutual information between attributes. Once this tree is constructed, TAN proceeds to calculate the conditional probabilities given a query point similar to Naive Bayes, but also factoring in the additional dependency information.

\paragraph{}
Given that TAN is a more advanced version of Naive Bayes, it is expected that it's performance should be at least as good as Naive Bayes. Additionally, because the assumption of independent attributes has been lifted, TAN should perform well on data sets that contain interdependent data points.

\subsection{Iterative Dichotimizer 3}
\paragraph{}
Iterative Dichotimizer 3 (ID3) is a decision tree-based learning method. It is an eager method that first computes a model (in this case, a decision tree) and then uses that model to quickly classify query points. The tree is composed of nodes representing tests on a single attribute at a time, with edges representing partitions of the data set along that attribute. The tree is constructed in such a way that in each subtree, a test of the most important attribute -- as determined by a gain function -- is selected for the root node. The leaf nodes of the tree are class labels. When given a query data point, ID3 simply traverses the tree based on the attributes in the query point until it arrives at a leaf node, assigning the query that class label.

\paragraph{}
Because ID3 selects the most important attribute test first for a given subset of the data, it should be good at generalizing. Additionally, smart pruning techniques should improve the accuracy even further. However, ID3 may struggle when there is no clear ``most important'' attribute in the data, i.e. all of the attributes are equally important in determining the class of a point.

\section{Design Decisions}
\subsection{Missing Attributes}
\paragraph{}
For handling missing values in the breast cancer data set, we decided to use data imputation. The attributes that are missing values are measured from 1-10, so we will substitute a uniformly chosen random value between 1 and 10 for those missing values. Because missing data in the congress vote data set is not truly missing values, we will handle the unknown values in the data by treating them as a third possible data value (in addition to yes and no). We decided to do this because a gap in the data set indicates that the congressman either abstained or missed the vote, and imputing values for the gaps could cause issues with the algorithms. 

\subsection{k-Nearest Neighbors}
\paragraph{}
For the k-Nearest Neighbor algorithm, we decided to use the unweighted version; we will not weigh votes by distance to the query point. Each of the k neighbors will contribute equally to the classification regardless of distance to the query or other factors. This is for the sake of simplicity and efficiency.

\paragraph{}
We will be using a k-d tree data structure to hasten the search for all nearby neighbors. This should provide a substantial boost over a simple sequential search.

\subsection{Naive Bayes}
\paragraph{}
We have decided that for the Naive Bayes algorithm, we will use Naive Density Estimation to estimate the probability of a value of a continuous attribute. This will entail tuning the h-parameter to ensure good results. We are doing that because it should provide a decent amount of accuracy when calculating these probabilities, while still being easy to calculate. 

\subsection{Tree Augmented Naive Bayes}
\paragraph{}
For Tree-Augmented Naive Bayes, the conditional mutual information function will analyze the data and find which attributes have very low variance between each other. Attributes with low relative variance will be marked as dependent on the focused-upon attributes. This will allow us to create an effective tree to run TAN on without manually analyzing the data.

\paragraph{}
Additionally, the decision to use Naive Density Estimation on continuous attributes will also apply to TAN as it did to Naive Bayes.

\subsection{Iterative Dichotimizer 3}
\paragraph{}
For ID3 we will be using uniform buckets to discretize continuous attributes. This approach is easy to implement and should be effective enough for our purposes.

\paragraph{}
We will be implementing a reduced error pruning technique to ensure that our decision trees are generalized and do not over-fit the data. This technique involves generating the tree to its fullest extent, then iteratively removing and generalizing subtrees where doing so reduces error when tested on a pruning set partition of the full data set.

\section{Experimental Method}
\paragraph{}
To evaluate the performance of the algorithms, we will be recording the results while using 5x2 cross validation. At the start of execution, we will split each of the data sets into two randomly selected, stratified partitions. The first set will be our training set - the data we use to establish the models in our algorithms. When that is established, we will run each algorithm on the second set, dubbed the test set. Results of accuracy will be measured from there. This process will be repeated 5 times in total.

\paragraph{}
We will measure accuracy in two ways. First, we will record the simple 0-1 loss of the algorithms as a quick measure of accuracy. Additionally, we will calculate the F1 Score of the results. This incorporates some sort of average of both precision and recall, and should give a fair measure of how well the algorithms perform in both areas. To make this calculation on the multi-class data sets, we will use a one vs. all approach to determine positives and negatives.

\section{References}
[1] S.  Russell, P.  Norvig and E.  Davis, \textit{Artificial Intelligence}. Upper Saddle River, NJ: Prentice Hall, 2010.\\[2pt]
[2] N. Friedman, D. Geiger, and M. Goldszmidt, "Bayesian Network Classifiers," \textit{Machine Learning}, no. 29, pp. 131–163, 1997.
\end{document}
