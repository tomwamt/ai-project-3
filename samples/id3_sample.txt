ID3 Decision Tree:

Loading House Votes data set...
Training on 348 points.
Pruning validation with 86 points.
Pruned Tree:
test on attribute 3
    = 0: test on attribute 11
        = 0: test on attribute 8
            = 0: republican
            = 1: democrat
            = -1: democrat
        = 1: republican
        = -1: democrat
    = 1: republican
    = -1: test on attribute 2
        = 0: democrat
        = 1: democrat
        = -1: test on attribute 11
            = 0: republican
            = 1: democrat
            = -1: democrat

Query Point: (1, -1, 1, -1, -1, -1, 1, 1, 1, -1, -1, -1, -1, -1, -1, 1)
ID3 classification: democrat
True class: democrat
